/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 16.0 		*/
/*  Created On : 14-nov.-2022 09:43:43 a. m. 				*/
/*  DBMS       : MySql 						*/
/* ---------------------------------------------------- */

SET FOREIGN_KEY_CHECKS=0
; 
/* Drop Tables */

DROP TABLE IF EXISTS `categorias` CASCADE
;

DROP TABLE IF EXISTS `clientes` CASCADE
;

DROP TABLE IF EXISTS `notificaciones` CASCADE
;

DROP TABLE IF EXISTS `operaciones` CASCADE
;

/* Create Tables */

CREATE TABLE `categorias`
(
	`id_categoria` INT NOT NULL,
	`title` VARCHAR(50) NOT NULL,
	`status` CHAR(1) NOT NULL DEFAULT 'A',
	CONSTRAINT `PK_categorias` PRIMARY KEY (`id_categoria` ASC)
)

;

CREATE TABLE `clientes`
(
	`id_cliente` INT NOT NULL,
	`firebase_hash` VARCHAR(50) NOT NULL,
	`status` CHAR(1) NOT NULL DEFAULT 'A',
	CONSTRAINT `PK_clientes` PRIMARY KEY (`id_cliente` ASC)
)

;

CREATE TABLE `notificaciones`
(
	`id_notificacion` INT NOT NULL,
	`id_operacion` INT NOT NULL,
	`id_cliente` INT NOT NULL,
	`title` VARCHAR(50) NOT NULL,
	`description` VARCHAR(50) NOT NULL,
	`status` CHAR(1) NOT NULL DEFAULT 'A',
	CONSTRAINT `PK_notificaciones` PRIMARY KEY (`id_notificacion` ASC)
)

;

CREATE TABLE `operaciones`
(
	`id_operacion` INT NOT NULL,
	`id_categoria` INT NOT NULL,
	`status` CHAR(1) NOT NULL DEFAULT 'A',
	CONSTRAINT `PK_operaciones` PRIMARY KEY (`id_operacion` ASC)
)

;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE `notificaciones` 
 ADD INDEX `IXFK_notificaciones_clientes` (`id_cliente` ASC)
;

ALTER TABLE `notificaciones` 
 ADD INDEX `IXFK_notificaciones_operaciones` (`id_operacion` ASC)
;

ALTER TABLE `operaciones` 
 ADD INDEX `IXFK_operaciones_categorias` (`id_categoria` ASC)
;

/* Create Foreign Key Constraints */

ALTER TABLE `notificaciones` 
 ADD CONSTRAINT `FK_notificaciones_clientes`
	FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `notificaciones` 
 ADD CONSTRAINT `FK_notificaciones_operaciones`
	FOREIGN KEY (`id_operacion`) REFERENCES `operaciones` (`id_operacion`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `operaciones` 
 ADD CONSTRAINT `FK_operaciones_categorias`
	FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`) ON DELETE Cascade ON UPDATE Cascade
;

SET FOREIGN_KEY_CHECKS=1
; 
