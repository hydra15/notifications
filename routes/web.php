<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\NotificacionController;
use App\Http\Controllers\OperacionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::apiResource('api/categoria', CategoriaController::class,      ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
Route::apiResource('api/operacion', OperacionController::class,      ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
Route::apiResource('api/notificacion', NotificacionController::class,   ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
Route::apiResource('api/cliente', ClienteController::class,        ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
