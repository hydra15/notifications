<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operacion extends Model {
    use HasFactory;

    protected $table = 'operaciones';
    protected $fillable = ['id_operacion', 'id_categoria', 'status'];
    protected $primaryKey = 'id_operacion';
}
