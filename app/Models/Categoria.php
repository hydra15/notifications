<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {
    use HasFactory;

    protected $table = 'categorias';
    protected $fillable = ['id_categoria', 'title', 'status'];
    protected $primaryKey = 'id_categoria';
}
